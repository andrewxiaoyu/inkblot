/* Params */
var opacityDelay = 1500;
var animationDelay = 300;

var developAnimation = 600;
var formAnimation = 300;

var controller = new ScrollMagic.Controller();

document.addEventListener('DOMContentLoaded', function(){
    var video = document.getElementById("ink");
    video.addEventListener("timeupdate", function(){
        if(this.currentTime >= 8) {
            this.pause();
        }
    });

    scrollAnimations();
}, false);

function updateAnimatedImg(index, imgDiv, delay){
    console.log(index);
    if(index == 0){
        imgDiv.children[index].style.display="inline-block";
    }
    else{
        imgDiv.children[index-1].style.display="none";
        imgDiv.children[index].style.display="inline-block";
    }
    if(index < imgDiv.children.length-1){
        setTimeout(function(){
            updateAnimatedImg(index+1, imgDiv, delay);
        }, delay);
    }
}

function scrollAnimations(){
    new ScrollMagic.Scene({
        triggerElement:"#idea-container",
        triggerHook: "onCenter",
        reverse:false
    })
    .on('start', function () {
        var draw = document.getElementById("idea-container");
        draw.children[1].children[0].children[1].style.width = "0";

        setTimeout(function(){
            draw.children[1].children[1].style.opacity = 1;
            draw.children[0].style.opacity = 1;
        }, opacityDelay);
    })
    .addTo(controller);

    new ScrollMagic.Scene({
        triggerElement:"#develop-container",
        triggerHook: "onCenter",
        reverse:false
    })
    .on('start', function () {
        var draw = document.getElementById("develop-container");
        draw.children[1].children[0].children[1].style.width = "0";

        setTimeout(function(){
            draw.children[1].children[1].style.opacity = 1;
            draw.children[0].style.opacity = 1;

            setTimeout(function(){
                updateAnimatedImg(0, document.getElementById("develop"), developAnimation);
            }, animationDelay);
        }, opacityDelay);
    })
    .addTo(controller);

    new ScrollMagic.Scene({
        triggerElement:"#form-container",
        triggerHook: "onCenter",
        reverse:false
    })
    .on('start', function () {
        var draw = document.getElementById("form-container");
        draw.children[1].children[0].children[1].style.width = "0";


        setTimeout(function(){
            draw.children[1].children[1].style.opacity = 1;
            draw.children[0].style.opacity = 1;

            setTimeout(function(){
                updateAnimatedImg(0, document.getElementById("form"), formAnimation);
            }, animationDelay);
        }, opacityDelay);
    })
    .addTo(controller);
}
