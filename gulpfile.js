var gulp = require('gulp');
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var minify = require('gulp-minifier');

gulp.task('default',  ['sass','pug','js','font','img','watch']);

gulp.task('watch', function(){
  gulp.watch('./dev/sass/*.scss', ['sass']);
  gulp.watch('./dev/font/*', ['font']);
  gulp.watch('./dev/img/**/*', ['img']);
  gulp.watch('./dev/js/*.js', ['js']);
	gulp.watch('./dev/*.pug', ['pug']);
});

gulp.task('js', function() {
  return gulp.src('./dev/js/*').pipe(minify({
    minify: true,
    minifyJS: {
      sourceMap: true
    },
    getKeptComment: function (content, filePath) {
        var m = content.match(/\/\*![\s\S]*?\*\//img);
        return m && m.join('\n') + '\n' || '';
    }
  })).pipe(gulp.dest('./docs/js'));
});

gulp.task('sass', function () {
  return gulp.src('./dev/sass/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(minify({
      minify: true,
      minifyCSS: true,
      getKeptComment: function (content, filePath) {
          var m = content.match(/\/\*![\s\S]*?\*\//img);
          return m && m.join('\n') + '\n' || '';
      }
    }))
    .pipe(gulp.dest('./docs/css'));
});

gulp.task('font', function() {
  return gulp.src('./dev/font/*').pipe(gulp.dest('./docs/font/'));
});
gulp.task('img', function() {
  return gulp.src('./dev/img/**/*').pipe(gulp.dest('./docs/img/'));
});

gulp.task('pug', function () {
  return gulp.src('./dev/*.pug')
    .pipe(pug())
    .pipe(gulp.dest('./docs/'));
});
